# README #

This Package is create cron job to do scheduled internet speed tests using the official ookla speed test cli 

### What is this package for? ###

* Scheduled internet speed tests
* It tests download speed, upload speed and ping
* It stores the result of the speed test into mongodb  

### How do I use it? ###

* Provide configuration JSON file of the following format:
```
{
  "mongo": {
    "host": "mongo-db-host",
    "port": "27017",
    "database": "db",
    "collection": "col",
    "username": "user",
    "password": "pass"
  },
  "scheduler": {
    "hours": [1],
    "minutes": [0]
  }
}
```
1. mongo config will be the configuration of mongodb server (username and password can be ommitted if it is not required)
2. scheduler contains hours and minuts which when we want to create the job (both of them can be ommitted)
    
* To execute it use `npm start <config file>` command with the config file, which can be the absolute path or relative path to the repo base directory.
* In linux you may get permissions errors when trying to execute speedtest cli to fix this you have to run `chmod +x speedtest-cli/linux/speedtest` where speedtest-cli/linux/speedtest is relative path to the cli from the repo based directory  
