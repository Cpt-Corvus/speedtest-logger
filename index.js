const schedule = require('node-schedule');

const SpeedTest = require('./src/speedtest');

if(process.argv.length < 3) {
    console.log('usage: npm start <config.json file>');
    process.exit(1);
}

let configFilePath = process.argv[2];
const config = require(configFilePath);

const scheduleTime = {second: 0};

if(config.scheduler.hours && config.scheduler.hours.length > 0) {
    if(config.scheduler.hours.length > 1)
        scheduleTime.hour = config.scheduler.hours;
    else scheduleTime.hour = config.scheduler.hours[0];
}

if(config.scheduler.minutes && config.scheduler.minutes.length > 0) {
    if(config.scheduler.minutes.length > 1)
        scheduleTime.minute = config.scheduler.minutes;
    else scheduleTime.minute = config.scheduler.minutes[0];
}

console.log('Will do a speed test if time is match: ', scheduleTime);

schedule.scheduleJob(scheduleTime, async () => {
    const now = new Date();
    console.log(`[${now}] start speed test:...`);
    await SpeedTest.doSpeedTest(config, now);
});
