
const MongoClient = require('mongodb').MongoClient;

let mongoClient;
let db;
let dbConfig;

async function connectToDB(config) {
    return new Promise((resolve, reject) => {
        if(!config || !config.mongo || !config.mongo.host)
            return reject("Failed: configuration not passed correctly");

        dbConfig = config.mongo;
        let url = `mongodb://${dbConfig.username && dbConfig.password ? dbConfig.username+':'+dbConfig.password + '@' : ''}${dbConfig.host}`;

        MongoClient.connect(url, function(err, client) {
            if(err) reject(err);
            else {
                mongoClient = client;
                db = client.db(dbConfig.database);
                resolve(db);
            }
        });
    });
}

function closeDBConnection() {
    mongoClient.close();
}

async function insertData(data) {
    return new Promise((resolve, reject) => {
        if(!mongoClient || !db) reject("Failed: not connected to mongodb");
        else {
            console.log(`[${new Date()}]: save data to database: Started`);
            const collection = db.collection(dbConfig.collection);
            collection.insertMany(data, (err, result) => {
               if(err)  reject(err);
               else {
                   console.log(`[${new Date()}]: save data to database: Done`);
                   resolve(result);
               }
            });
        }
    });
}

module.exports = {
    connectToDB,
    closeDBConnection,
    insertData
};
