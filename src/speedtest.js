
const DB = require('./db');
const { exec } = require("child_process");

const speedtestCLICommand = process.platform==='win32' ? 'speedtest-cli\\windows\\speedtest.exe' : 'speedtest-cli/linux/speedtest';

async function doSpeedTest(config, date) {
    return new Promise((resolve, reject) => {
        exec(`${speedtestCLICommand} --progress=no`, async (err, out) => {
            if(out) {
                console.log(`[${new Date()}] speedtest done! ${out}`);
                await DB.connectToDB(config);
                await DB.insertData([getSpeedTestResultInfo(out, date)]);
                DB.closeDBConnection();
                resolve();
            } else {
                console.log(`Error while doing speed test: ${err}`);
                reject(err);
            }
        });
    })
}

function getSpeedTestResultInfo(result, date) {
    let arr = result.split('\n');
    if(arr.length < 8)
        return undefined;
    return {
        date: date.toString(),
        ping: arr[5].trim().split(/[ ]+/)[1],
        download: arr[6].trim().split(/[ ]+/)[1],
        upload: arr[7].trim().split(/[ ]+/)[1],
        url: arr.length >= 10 ? arr[9].trim().split(/[ ]+/)[2] : undefined
    };
}

module.exports = {
    doSpeedTest
};
